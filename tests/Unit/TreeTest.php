<?php

use App\Tree;
use PHPUnit\Framework\TestCase;

class TreeTest extends TestCase
{
    public function testBuidChildren()
    {
        $tree = new Tree();
        $tree->addNode(1, "Root node", null);
        $tree->addNode(2, "ChildNode", 1);
        $tree->addNode(3, "GrandChildNode", 2);

        $tree->addNode(4, "2nd child", 1);

        $build = $tree->getChildren(1);
        $this->assertEquals("2nd child", $build[4]['data']);
        $this->assertEquals("GrandChildNode", $build[2]['children'][3]['data']);
    }
}
