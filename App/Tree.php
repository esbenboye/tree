<?php

namespace App;

class Tree
{
    private $nodes = [];

    /**
     * @param int $id
     * @param mixed $data
     * @param mixed|null $parent
     * @return void
     */
    public function addNode(int $id, $data, ?int $parent = null): void
    {
        $this->nodes[$id]['data'] = $data;
        $this->nodes[$id]['parent'] = $parent;
    }

    /**
     * @param int $nodeId
     * @return void
     */
    public function removeNode(int $nodeId): void
    {
        unset($this->nodes[$nodeId]);
    }

    /**
     * @param int $parent
     * @return array
     */
    public function getChildren(int $parent): array
    {
        $data = [];
        foreach ($this->nodes as $nodeId => $node) {
            if ($node['parent'] == $parent) {
                $data[$nodeId] = [
                    'data' => $node['data'],
                    'children' => $this->getChildren($nodeId)
                ];
            }
        }
        return $data;
    }

    /**
     * @param int $nodeId
     * @param mixed $newData
     * @return void
     */
    public function updateData(int $nodeId, $newData): void
    {
        if (isset($this->nodes[$nodeId])) {
            $this->nodes[$nodeId]['data'] = $newData;
        }
    }

    /**
     * @param int $nodeId
     * @param mixed $newParent
     * @return void
     */
    public function updateParent(int $nodeId, $newParent): void
    {
        if (isset($this->nodes[$nodeId])) {
            $this->nodes[$nodeId]['parent'] = $newParent;
        }
    }

    /** @return array  */
    public function getNodes():array
    {
        return $this->nodes;
    }
}
