<?php

use App\Tree;

require("./vendor/autoload.php");

$tree = new Tree();

$tree->addNode(1, "FastFoods", null);
$tree->addNode(2, "McDonald", 1);
$tree->addNode(3, "Burger King", 1);
$tree->addNode(4, "BK 01", 3);
$tree->addNode(5, "McD01", 2);
$tree->addNode(6, "Subway", 1);

$tree->updateParent(5, 3); // Updating MCD01 to have parent BK

var_dump($tree->getChildren(1));

